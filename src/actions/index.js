import Axios from "axios";
import { Buffer } from "buffer";

const BASE_URL = "http://52.74.166.134:3000";

export const handleRegister = (value) => {
  return (dispatch) => {
    dispatch({
      type: "REGISTER_ERROR",
      payload: {},
    });
    dispatch({
      type: "REGISTER_SUCCESS",
      payload: {},
    });

    Axios.post(BASE_URL + "/api/signup", value)
      .then((response) => {
        const payload = response.data.response;
        dispatch({
          type: "REGISTER_SUCCESS",
          payload,
        });
      })
      .catch((error) => {
        const payload = error.response.data.response;
        dispatch({
          type: "REGISTER_ERROR",
          payload,
        });
        throw error;
      });
  };
};

export const handleLogin = (value) => {
  return (dispatch) => {
    dispatch({
      type: "LOGIN_ERROR",
      payload: {},
    });
    dispatch({
      type: "LOGIN_SUCCESS",
      payload: {},
    });

    const token = `${value.user_name}:${value.password}`;
    const encodedToken = Buffer.from(token).toString("base64");
    const headers = { Authorization: "Basic " + encodedToken };

    Axios.post(
      BASE_URL + "/api/login",
      {},
      {
        headers,
      }
    )
      .then((response) => {
        const payload = response.data.response;
        dispatch({
          type: "LOGIN_SUCCESS",
          payload,
        });
      })
      .catch((error) => {
        const payload = error.response.data.response;
        dispatch({
          type: "LOGIN_ERROR",
          payload,
        });
        throw error;
      });
  };
};

export const getListGPS = (loginToken) => {
  return (dispatch) => {
    Axios.defaults.headers.common["Authorization"] = "Bearer " + loginToken;
    return Axios.get(BASE_URL + "/api")
      .then((response) => {
        const payload = response.data;
        dispatch({
          type: "LIST_GPS",
          payload,
        });
      })
      .catch((error) => {
        const payload = error.data;
        dispatch({
          type: "LIST_ERROR",
          payload,
        });
        throw err;
      });
  };
};

export const getDetailGPS = (loginToken, value) => {
  return (dispatch) => {
    Axios.defaults.headers.common["Authorization"] = "Bearer " + loginToken;
    return Axios.get(BASE_URL + `/api/${value}`)
      .then((response) => {
        const payload = response.data;
        dispatch({
          type: "DETAIL_GPS",
          payload,
        });
      })
      .catch((error) => {
        const payload = error.data;
        dispatch({
          type: "DETAIL_ERROR",
          payload,
        });
        throw err;
      });
  };
};
