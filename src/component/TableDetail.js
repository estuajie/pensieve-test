import React, { useState, useEffect } from "react";
import { Button, Table } from "react-bootstrap";
import Moment from "react-moment";

const TableDetail = (p) => {
  const { dataTableDetail } = p;

  return (
    <div>
      <Table striped>
        <thead>
          <tr>
            <th>#</th>
            <th>Timestamp</th>
            <th>Location</th>
          </tr>
        </thead>
        <tbody>
          {dataTableDetail.map((data) => {
            return (
              <tr key={data.id}>
                <td>{data.id}</td>
                <td>
                  <Moment format="YYYY-MM-DD HH:mm">{data.timestamp}</Moment>{" "}
                </td>
                <td>{data.location}</td>
              </tr>
            );
          })}
        </tbody>
      </Table>
    </div>
  );
};

export default TableDetail;
