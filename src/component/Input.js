import React, { useState, useEffect } from "react";
import { Form } from "react-bootstrap";

const Input = (p) => {
  const {
    dataRegister,
    setDataRegister,
    dataLogin,
    setDataLogin,
    label,
    type,
    placeholder,
    feedback,
    inputFor,
    route,
  } = p;

  const handleTypingRegister = (e) => {
    if (inputFor === "name") {
      setDataRegister({
        ...dataRegister,
        name: e.target.value,
      });
    } else if (inputFor === "email") {
      setDataRegister({
        ...dataRegister,
        email: e.target.value,
      });
    } else {
      setDataRegister({
        ...dataRegister,
        password: e.target.value,
      });
    }
  };

  const handleTypingLogin = (e) => {
    if (inputFor === "username") {
      setDataLogin({
        ...dataLogin,
        user_name: e.target.value,
      });
    } else {
      setDataLogin({
        ...dataLogin,
        password: e.target.value,
      });
    }
  };

  return (
    <div>
      <Form.Group className="mb-3">
        <Form.Label>{label}</Form.Label>
        <Form.Control
          type={type}
          placeholder={placeholder}
          required
          onChange={(e) => {
            if (route === "register") {
              handleTypingRegister(e);
            } else {
              handleTypingLogin(e);
            }
          }}
        />
        <Form.Control.Feedback type="invalid">{feedback}</Form.Control.Feedback>
      </Form.Group>
    </div>
  );
};

export default Input;
