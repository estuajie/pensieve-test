import React from "react";
import { Button } from "react-bootstrap";

const ButtonBack = (p) => {
  const { goBack } = p;

  return (
    <div>
      <Button
        className="mb-1"
        size="sm"
        variant="outline-secondary"
        type=""
        onClick={() => goBack()}
      >
        Back
      </Button>
    </div>
  );
};

export default ButtonBack;
