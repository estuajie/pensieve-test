import React, { useState, useEffect } from "react";
import { Button, Table } from "react-bootstrap";
import Moment from "react-moment";

const TableList = (p) => {
  const { dataTable, gotoDetail, text } = p;

  return (
    <div>
      <Table striped>
        <thead>
          <tr>
            <th>#</th>
            <th>Device ID</th>
            <th>Device Type</th>
            <th>Location</th>
            <th>Timestamp</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {dataTable.map((data) => {
            return (
              <tr key={data.id}>
                <td>{data.id}</td>
                <td>{data.device_id}</td>
                <td>{data.device_type}</td>
                <td>{data.location}</td>
                <td>
                  <Moment format="YYYY-MM-DD HH:mm">{data.timestamp}</Moment>{" "}
                </td>
                <td>
                  <Button size="sm" onClick={() => gotoDetail(data.device_id)}>
                    Detail
                  </Button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </Table>
    </div>
  );
};

export default TableList;
