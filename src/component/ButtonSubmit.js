import React from "react";
import { Button } from "react-bootstrap";

const ButtonSubmit = (p) => {
  const { isLoading, text } = p;

  return (
    <div>
      <Button
        variant="primary"
        type="submit"
        disabled={isLoading}
        className="w-100"
      >
        {isLoading ? "Loading…" : `${text}`}
      </Button>
    </div>
  );
};

export default ButtonSubmit;
