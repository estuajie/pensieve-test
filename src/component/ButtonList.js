import React from "react";
import { Button } from "react-bootstrap";

const ButtonList = (p) => {
  const { disablePrev, changeList, text } = p;

  const handleList = () => {
    if (text === "Prev") {
      changeList("prev");
    } else {
      changeList("next");
    }
  };

  return (
    <div>
      <Button
        variant="outline-primary"
        size="sm"
        onClick={() => handleList()}
        disabled={disablePrev}
      >
        {text}
      </Button>
    </div>
  );
};

export default ButtonList;
