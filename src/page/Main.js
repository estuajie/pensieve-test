import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Container, Button, Form } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { handleLogin } from "../actions";
import { useCookies } from "react-cookie";

import Input from "../component/Input";
import ButtonSubmit from "../component/ButtonSubmit";

const Main = () => {
  const [dataLogin, setDataLogin] = useState({
    user_name: "",
    password: "",
  });
  const [validated, setValidated] = useState(false);
  const [message, setMessage] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [cookie, setCookie, removeCookie] = useCookies(["loginToken"]);

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const loginReducer = useSelector((state) => state.loginReducer);

  useEffect(() => {
    if (loginReducer.error?.status === "Unauthorized") {
      setMessage(loginReducer.error.message);
    } else if (loginReducer.success?.status === "OK") {
      setCookie("loginToken", loginReducer.success.message.loginToken);
      navigate("/list");
    }
    setIsLoading(false);
  }, [loginReducer]);

  const handleSubmit = (event) => {
    event.preventDefault();
    const form = event.currentTarget;
    setValidated(true);
    if (form.checkValidity() !== false) {
      setMessage("");
      setIsLoading(true);
      dispatch(handleLogin(dataLogin));
    }
  };

  const goRegister = () => {
    navigate(`/register`);
  };

  const p = {
    dataLogin,
    setDataLogin,
    validated,
    setValidated,
    message,
    setMessage,
    isLoading,
    setIsLoading,
  };

  return (
    <Container>
      <Form
        noValidate
        validated={validated}
        onSubmit={handleSubmit}
        className="position-absolute top-50 start-50 translate-middle w-25 main"
      >
        <h2 className="mb-0">Welcome</h2>
        <p className="mb-5">Please login or register to see more data</p>
        <div className="d-grid gap-2">
          <Input
            {...p}
            label={"Name"}
            type={"name"}
            placeholder={"Enter your name"}
            feedback={"Please choose a username."}
            route={"login"}
            inputFor={"username"}
          />
          <Input
            {...p}
            label={"Password"}
            type={"password"}
            placeholder={"Enter your Password"}
            feedback={"Please type password."}
            route={"login"}
            inputFor={"password"}
          />

          <ButtonSubmit {...p} text={"Login"} />

          <p>{message}</p>
          <div>
            <p>
              {"Don't have account? "}
              <strong className="pointer" onClick={() => goRegister()}>
                Register here
              </strong>
            </p>
          </div>
        </div>
      </Form>
    </Container>
  );
};

export default Main;
