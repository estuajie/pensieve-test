import React, { useState, useEffect } from "react";
import { Container, Row, Col, Button, Table } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { PieChart } from "react-minimal-pie-chart";
import { useCookies } from "react-cookie";
import { getDetailGPS } from "../actions";
import { useSearchParams } from "react-router-dom";
import Moment from "react-moment";
import { useNavigate } from "react-router-dom";

import TableDetail from "../component/TableDetail";
import ButtonBack from "../component/ButtonBack";

const Detail = (props) => {
  const [cookie] = useCookies(["loginToken"]);
  const [searchParams] = useSearchParams();
  const [dataMock, setDataMock] = useState([]);
  const [titleDetail, setTitleDetail] = useState([]);

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const detailGpsReducer = useSelector((state) => state.detailGpsReducer);

  const defaultLabelStyle = {
    fontSize: "10px",
    fontWeight: "bold",
  };
  const device_id = searchParams.get("device_id");
  const goBack = () => {
    navigate(`/list`);
  };

  useEffect(() => {
    var myData = [];
    var addColor = [];
    detailGpsReducer.detail.forEach(function (e) {
      myData.push({
        title: e.location,
        value: 20,
      });
    });
    let s = myData.reduce((acc, obj) => {
      acc.set(obj.title, (acc.get(obj.title) || 0) + obj.value);
      return acc;
    }, new Map());

    // turn the Map into something that will display:
    let dataNew = [...s.entries()].map(([title, value]) => ({
      title,
      value,
    }));

    // adding color
    dataNew.forEach(function (e) {
      addColor.push({
        ...e,
        color:
          e.title === "L1"
            ? "#E38627"
            : e.title === "L2"
            ? "#C13C37"
            : e.title === "L3"
            ? "#198754"
            : e.title === "L4"
            ? "#0d6efd"
            : e.title === "L5"
            ? "#c0ff00"
            : "#e98afe",
      });
    });
    setDataMock(addColor);
    setTitleDetail(detailGpsReducer.detail[0]?.device_type);
  }, [detailGpsReducer.detail]);

  useEffect(() => {
    dispatch(getDetailGPS(cookie.loginToken, device_id));
  }, []);

  return (
    <Container className="position-absolute top-50 start-50 translate-middle w-100 main">
      <Row className="mb-4">
        <Col>
          <ButtonBack goBack={goBack} />
        </Col>
        <h2 className="mb-0">{device_id}</h2>
        <h5>{titleDetail}</h5>
      </Row>
      <Row>
        <Col sm="4">
          <TableDetail dataTableDetail={detailGpsReducer.detail} />
        </Col>
        <Col sm="4">
          <PieChart
            data={dataMock}
            label={({ dataEntry }) => Math.round(dataEntry.percentage) + "%"}
            animate={true}
            style={{ height: "240px" }}
            labelStyle={{
              ...defaultLabelStyle,
            }}
          />
        </Col>
        <Col sm="4">
          <h5 className="mb-4 mt-4">Time Spent on each location</h5>
          {dataMock.map((data, index) => {
            return (
              <ul>
                <li key={index}>
                  <div>
                    <div className={"css-" + data.title}></div>
                    <strong>{data.title}</strong>
                  </div>
                  <p>Percentage: {data.value}%</p>
                </li>
              </ul>
            );
          })}
        </Col>
      </Row>
    </Container>
  );
};

export default Detail;
