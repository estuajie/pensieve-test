import React, { useState, useEffect } from "react";
import { Container, Row, Col, Button, Form } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { handleRegister } from "../actions";

import Input from "../component/Input";
import ButtonSubmit from "../component/ButtonSubmit";
import ButtonBack from "../component/ButtonBack";

const Register = () => {
  const [dataRegister, setDataRegister] = useState({
    name: "",
    email: "",
    password: "",
  });
  const [validated, setValidated] = useState(false);
  const [message, setMessage] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [succesRegister, setSuccesRegister] = useState(false);

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const goBack = () => {
    navigate(`/`);
  };

  const registerReducer = useSelector((state) => state.registerReducer);

  useEffect(() => {
    if (registerReducer.error?.status === "Bad Request") {
      setMessage(registerReducer.error.message);
    } else if (registerReducer.success?.status === "OK") {
      setMessage(registerReducer.success.message);
      setSuccesRegister(true);
    }
    setIsLoading(false);
  }, [registerReducer]);

  const handleSubmit = (event) => {
    event.preventDefault();
    const form = event.currentTarget;
    setValidated(true);
    if (form.checkValidity() !== false) {
      setMessage("");
      setIsLoading(true);
      dispatch(handleRegister(dataRegister));
    }
  };

  const p = {
    dataRegister,
    setDataRegister,
    validated,
    setValidated,
    message,
    setMessage,
    isLoading,
    setIsLoading,
    succesRegister,
    setSuccesRegister,
  };

  return (
    <Container>
      {succesRegister ? (
        <div className="position-absolute top-50 start-50 translate-middle w-25 main">
          <p>{message}</p>
          <Button variant="primary" type="submit" onClick={() => goBack()}>
            Login
          </Button>
        </div>
      ) : (
        <Form
          noValidate
          validated={validated}
          onSubmit={handleSubmit}
          className="position-absolute top-50 start-50 translate-middle w-25 main"
        >
          <ButtonBack goBack={goBack} />
          <h2 className="mb-0">Register</h2>
          <p className="mb-5">Please choose your name, email & password</p>
          <div className="d-grid gap-2">
            <Input
              {...p}
              label={"Name"}
              type={"name"}
              placeholder={"Enter your name"}
              feedback={"Please choose a username."}
              route={"register"}
              inputFor={"name"}
            />

            <Input
              {...p}
              label={"Email address"}
              type={"email"}
              placeholder={"Enter your email"}
              feedback={"Please typing your email."}
              route={"register"}
              inputFor={"email"}
            />

            <Input
              {...p}
              label={"Password"}
              type={"password"}
              placeholder={"Password"}
              feedback={"Please type your password."}
              route={"register"}
              inputFor={"password"}
            />

            <ButtonSubmit {...p} text={"Register"} />

            <p>{message}</p>
          </div>
        </Form>
      )}
    </Container>
  );
};

export default Register;
