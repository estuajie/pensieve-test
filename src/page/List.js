import React, { useState, useEffect } from "react";
import { Container, Row } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { getListGPS } from "../actions";
import { useCookies } from "react-cookie";

import ButtonList from "../component/ButtonList";
import TableList from "../component/TableList";

const List = () => {
  const [cookie] = useCookies(["loginToken"]);
  const [dataTable, setDataTable] = useState([]);
  const [pagination, setPagination] = useState({
    prev: 0,
    next: 5,
  });
  const [disablePrev, setdisablePrev] = useState(true);
  const [disableNext, setdisableNext] = useState(false);

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const listGpsReducer = useSelector((state) => state.listGpsReducer);

  const changeList = (action) => {
    let allDataLength = listGpsReducer.list.length;
    if (action === "next") {
      setdisablePrev(false);
      let prev = pagination.prev + 5;
      let next = pagination.next + 5;
      setDataTable(listGpsReducer.list.slice(prev, next));
      setPagination({ prev: prev, next: next });
      if (next >= allDataLength) {
        setdisableNext(true);
      }
    } else {
      setdisableNext(false);
      let prev = pagination.prev - 5;
      let next = pagination.next - 5;
      setDataTable(listGpsReducer.list.slice(prev, next));
      setPagination({ prev: prev, next: next });
      if (pagination.prev === 5) {
        setdisablePrev(true);
      }
    }
  };

  const gotoDetail = (device_id) => {
    navigate(`/detail?device_id=${device_id}`);
  };

  useEffect(() => {
    dispatch(getListGPS(cookie.loginToken));
  }, []);

  useEffect(() => {
    setDataTable(listGpsReducer.list.slice(0, 5));
    setPagination({ prev: 0, next: 5 });
  }, [listGpsReducer]);

  const p = {
    disablePrev,
    setdisablePrev,
    disableNext,
    setdisableNext,
    changeList,
    dataTable,
    gotoDetail,
  };

  return (
    <Container className="position-absolute top-50 start-50 translate-middle w-100 main">
      <Row className="d-flex bd-highlight mb-4">
        <div className="d-flex bd-highlight">
          <div className="p-1 flex-grow-1 bd-highlight">
            <h2>Data List GPS</h2>
          </div>
          <div className="mt-4 p-1 bd-highlight">
            <p>
              show {pagination.prev === 0 ? "1" : pagination.prev + 1} {"- "}
              {pagination.next >= listGpsReducer.list.length
                ? listGpsReducer.list.length
                : pagination.next}{" "}
              {"of "}
              {listGpsReducer.list.length}
            </p>
          </div>
          <div className="mt-3 p-1 bd-highlight">
            <ButtonList {...p} disablePrev={disablePrev} text={"Prev"} />
          </div>
          <div className="mt-3 p-1 bd-highlight">
            <ButtonList {...p} disablePrev={disableNext} text={"Next"} />
          </div>
        </div>
      </Row>
      <TableList {...p} />
    </Container>
  );
};

export default List;
