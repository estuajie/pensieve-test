import { combineReducers } from "redux";

export const registerReducer = (
  state = {
    success: {},
    error: {},
  },
  action
) => {
  switch (action.type) {
    case "REGISTER_SUCCESS":
      return {
        ...state,
        success: action.payload,
      };
    case "REGISTER_ERROR":
      return {
        ...state,
        error: action.payload,
      };
    default:
      return state;
  }
};

export const loginReducer = (
  state = {
    success: {},
    error: {},
  },
  action
) => {
  switch (action.type) {
    case "LOGIN_SUCCESS":
      return {
        ...state,
        success: action.payload,
      };
    case "LOGIN_ERROR":
      return {
        ...state,
        error: action.payload,
      };
    default:
      return state;
  }
};

export const listGpsReducer = (
  state = {
    list: [],
    error: {},
  },
  action
) => {
  switch (action.type) {
    case "LIST_GPS":
      return {
        ...state,
        list: action.payload,
      };
    case "LIST_ERROR":
      return {
        ...state,
        error: action.payload,
      };
    default:
      return state;
  }
};

export const detailGpsReducer = (
  state = {
    detail: [],
    error: {},
  },
  action
) => {
  switch (action.type) {
    case "DETAIL_GPS":
      return {
        ...state,
        detail: action.payload,
      };
    case "DETAIL_ERROR":
      return {
        ...state,
        error: action.payload,
      };
    default:
      return state;
  }
};

const rootReducer = combineReducers({
  registerReducer: registerReducer,
  loginReducer: loginReducer,
  listGpsReducer: listGpsReducer,
  detailGpsReducer: detailGpsReducer,
});

export default rootReducer;
