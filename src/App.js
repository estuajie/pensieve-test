import "bootstrap/dist/css/bootstrap.min.css";
import Main from "./page/Main";
import "./style.scss";

function App() {
  return (
    <div className="App">
      <Main />
    </div>
  );
}

export default App;
